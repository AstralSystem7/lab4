﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MyGame;

public class Game1 : Game
{
    private GraphicsDeviceManager _graphics;
    private SpriteBatch _spriteBatch;
    private Texture2D _playerTexture;
    private SoundEffect _moveSound;
    private Vector2 _playerPosition;
    private SoundEffectInstance _moveSoundInstance;

    public Game1()
    {
        _graphics = new GraphicsDeviceManager(this);
        Content.RootDirectory = "Content";
        IsMouseVisible = true;
    }

    protected override void Initialize()
    {
        _playerPosition = new Vector2(_graphics.PreferredBackBufferWidth / 2, _graphics.PreferredBackBufferHeight / 2);

        base.Initialize();
    }

    protected override void LoadContent()
    {
        _spriteBatch = new SpriteBatch(GraphicsDevice);

        // Carrega o sprite
        _playerTexture = Content.Load<Texture2D>("Player");

        // Carrega o som
        _moveSound = Content.Load<SoundEffect>("Movimento");
        _moveSoundInstance = _moveSound.CreateInstance();
    }

    protected override void Update(GameTime gameTime)
    {
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            Exit();

        var keyboardState = Keyboard.GetState();

        if (keyboardState.IsKeyDown(Keys.Right))
        {
            _playerPosition.X += 5;
            _moveSoundInstance.Play();
        }
        if (keyboardState.IsKeyDown(Keys.Left))
        {
            _playerPosition.X -= 5;
            _moveSoundInstance.Play();
        }
        if (keyboardState.IsKeyDown(Keys.Up))
        {
            _playerPosition.Y -= 5;
            _moveSoundInstance.Play();
        }
        if (keyboardState.IsKeyDown(Keys.Down))
        {
            _playerPosition.Y += 5;
            _moveSoundInstance.Play();
        }

        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
        GraphicsDevice.Clear(Color.CornflowerBlue);

        _spriteBatch.Begin();
        _spriteBatch.Draw(_playerTexture, _playerPosition, Color.White);
        _spriteBatch.End();

        base.Draw(gameTime);
    }
}
